//////////////////////////////////////////////////////////////////
//
// Makerspace Adelaide
// Word Clock 11x10 + 4
// 
// Original code by John Missikos and Damien Brombal (c) 2013
// Updated in 2020 by Abigale Raeck for Photon Particle
// with major refactoring
//
// Libraries used include:
// NeoPixel Ring simple sketch (c) 2013 Shae Erisson, Adafruit
///////////////////////////////////////////////////////////////////

#include <neopixel.h>

#define PIN_BTN_TIME    D4
#define PIN_BTN_COLOUR  D3 
#define PIN_NEO_PIXEL   A5
#define NUM_PIXELS      114

bool state_test = false;

Adafruit_NeoPixel pixels(NUM_PIXELS, PIN_NEO_PIXEL, WS2812B);

uint8_t colourIndex = 0;
uint32_t colours[] = { pixels.Color(0,   0,   255),
                       pixels.Color(127, 127, 0),
                       pixels.Color(127, 0,   127),
                       pixels.Color(0,   127, 127),
                       pixels.Color(255, 0,   0),
                       pixels.Color(0,   255, 0) };

uint32_t setColour = colours[0];

uint8_t minuteSet = 0;
uint8_t hourSet   = 0;

time_t  nextEventTime;
#define EVENT_TIME_DELTA 60*2

time_t nextTimeSync;
#define TIME_SYNC_DELTA 60*60*24; //every 24 hours

/*
         I T E I S F T L V N E   ↰ 103
 92  ↱   J Q U A R T E R C K O   ↑ 102
 91  ↑   T W E N T Y X F I V E   ↰ 81
 70  ↱   H A L f C T E N E T O   ↑ 80
 69  ↑   P A S T B S E V E N L   ↰ 59
 48  ↱   O N E T W O T H R E E   ↑ 58
 47  ↑   F O U R F I V E S I X   ↰ 37
 26  ↱   N I N E K T W E L V E   ↑ 36
 25  ↑   E I G H T E L E V E N   ↰ 15
 4   →   T E N P Y O C L O C K   ↑ 14
     →          • • • •
*/

#define DEFINE_WORD(word, ...) const uint8_t word[] = {__VA_ARGS__};
#define SET_DISPLAY_WORD(word) for (uint8_t i=0; i < sizeof(word)/sizeof(uint8_t); ++i) pixels.setPixelColor((word)[i], setColour);
#define CASE_WORD(no, word)    case no: SET_DISPLAY_WORD(word) break;

DEFINE_WORD(W_ITIS,     113, 112, 110, 109)
DEFINE_WORD(W_OCLOCK,   9,   10,  11,  12,  13, 14)
DEFINE_WORD(W_TO,       79,  80)
DEFINE_WORD(W_PAST,     66,  67,  68,  69)

DEFINE_WORD(W_QUARTER,  93,  94,  95,  96,  97, 98, 99)
DEFINE_WORD(W_MFIVE,    84,  83,  82,  81)
DEFINE_WORD(W_MTEN,     75,  76,  77)
DEFINE_WORD(W_TWENTY,   91,  90,  89,  88,  87, 86)
DEFINE_WORD(W_HALF,     70,  71,  72,  73)

DEFINE_WORD(W_ONE,      48,  49,  50)
DEFINE_WORD(W_TWO,      51,  52,  53)
DEFINE_WORD(W_THREE,    54,  55,  56,  57,  58)
DEFINE_WORD(W_FOUR,     47,  46,  45,  44)
DEFINE_WORD(W_FIVE,     43,  42,  41,  40)
DEFINE_WORD(W_SIX,      39,  38,  37)
DEFINE_WORD(W_SEVEN,    64,  63,  62,  61,  60)
DEFINE_WORD(W_EIGHT,    25,  24,  23,  22,  21)
DEFINE_WORD(W_NINE,     26,  27,  28,  29)
DEFINE_WORD(W_TEN,      4,   5,   6)
DEFINE_WORD(W_ELEVEN,   20,  19,  18,  17,  16, 15)
DEFINE_WORD(W_TWELVE,   31,  32,  33,  34,  35, 36)

DEFINE_WORD(W_DOTS,      0,   1,   2,  3)

//Fill is missing from this Adafruit Library. DIY then...
#define FILL(c)  for (uint8_t  i=0; i<NUM_PIXELS; ++i) pixels.setPixelColor(i, c);
#define UPDATE() for (uint16_t i=0; i<200; i++) { pixels.show(); delay(3); }
#define QUICK_SHOW(word) pixels.clear(); SET_DISPLAY_WORD(word) pixels.show(); delay(500);


void setup() {
  /*
    Currently unable to flash external lights during Particle.connect().
    SYSTEM_MODE(SEMI_AUTOMATIC); allows lights to be set before hand, 
    but all interupts appear to be blocked, both software and hardware. 
    (what about attachInteruptDirect?) ¯\_(ツ)_/¯

    Just flash the lights during time sync at the start. 
  */
  pinMode(PIN_BTN_TIME, INPUT);
  pinMode(PIN_BTN_COLOUR, INPUT);
    
  Particle.syncTime();
  Particle.variable("time", Time.now);
  Particle.variable("DST", [](){ return Time.isDST() != 0;});
  Particle.variable("colour", [](){char buff[10]{}; sprintf(buff, "0x%06lX", setColour); return String(buff);});
  Particle.function("colour", *[](String s) -> int { 
    char* pos;

    uint32_t res{strtoul(s.c_str(), &pos, 16)};
    
    //has parse numbers?
    if (s.c_str() == pos) {
      return -1;
    } else {
      setColour = res;
      return 0;
    }
  });
  Particle.function("DST", *[](String s) -> int { 
    if (s.length() >= 1) {
      switch (s[0]) {
        case 'T':
        case 't':
          Time.beginDST();
          return 0;
        case 'F':
        case 'f':
          Time.endDST();
          return 0;
      }
    }

    return -1;
  });
  
  pixels.begin();
  pixels.clear();
  pixels.show();

  while (!Particle.syncTimeDone()) {
    for (uint8_t i=0; i < sizeof(W_DOTS)/sizeof(uint8_t); ++i) 
      pixels.setPixelColor(W_DOTS[i], pixels.Color(random(0,200),random(0,200),random(0,200)));

    pixels.show();
    delay(100);
  }
  Time.zone(+9.30);
  Time.beginDST();

  Particle.publish("Start Time", Time.timeStr(), PRIVATE);
  nextTimeSync  = Time.now() + TIME_SYNC_DELTA;
  nextEventTime = Time.now() + EVENT_TIME_DELTA;
}

inline void timeButtonPressed() {
  if (Time.isDST()) 
    Time.endDST();
  else 
    Time.beginDST();
}

inline void colourButtonPressed() {
  if (++colourIndex >= (sizeof(colours)/sizeof(uint32_t))) 
    colourIndex = 0;
  
  setColour = colours[colourIndex];
}

void loop() {
  {
    const bool btn_colour = digitalRead(PIN_BTN_COLOUR);
    const bool btn_time   = digitalRead(PIN_BTN_TIME);
    
    if (btn_colour && btn_time)
      state_test ^= true;
    else if (btn_colour) 
      colourButtonPressed();
    else if (btn_time)
      timeButtonPressed();
  }
  
  if (state_test) {
    FILL(pixels.Color(255,0,0))
    UPDATE()
    
    FILL(pixels.Color(0,255,0))
    UPDATE()
    
    FILL(pixels.Color(0,0,255))
    UPDATE()

    QUICK_SHOW(W_ITIS)    QUICK_SHOW(W_OCLOCK) QUICK_SHOW(W_TO)    QUICK_SHOW(W_PAST)
    QUICK_SHOW(W_QUARTER) QUICK_SHOW(W_MFIVE)  QUICK_SHOW(W_MTEN)  QUICK_SHOW(W_TWENTY) QUICK_SHOW(W_HALF)
    QUICK_SHOW(W_ONE)     QUICK_SHOW(W_TWO)    QUICK_SHOW(W_THREE) QUICK_SHOW(W_FOUR)   QUICK_SHOW(W_FIVE)
    QUICK_SHOW(W_SIX)     QUICK_SHOW(W_SEVEN)  QUICK_SHOW(W_EIGHT) QUICK_SHOW(W_NINE)   QUICK_SHOW(W_TEN)
    QUICK_SHOW(W_ELEVEN)  QUICK_SHOW(W_TWELVE)

    return;
  }

  {
    const time_t now = Time.now();
    
    if (now > nextEventTime) {
      nextEventTime += EVENT_TIME_DELTA;
      
      Particle.publish("Current Time Update", Time.timeStr(), PRIVATE);
    }

    // 24 hour time resync
    if (now > nextTimeSync) {
      nextTimeSync += TIME_SYNC_DELTA;
      
      Particle.syncTime();
      while (!Particle.syncTimeDone()) delay(20);
    }
  }
  
  //   ===================================================================
  //                          Display Clock Words
  //   ===================================================================

  pixels.clear();
    
  int inputMinute = Time.minute() + 12; // Particle servers are out by about 12 mins
  int inputHour   = Time.hour();
  
  //IT IS is always on
  SET_DISPLAY_WORD(W_ITIS)
  
  //Minute Serial Conversion
  if (inputMinute < 5) {            //??:00 --> ??:04
    SET_DISPLAY_WORD(W_OCLOCK)
  }
  else if (inputMinute < 35) {      //??:05 --> ??:34
    SET_DISPLAY_WORD(W_PAST)
    if (inputMinute >= 30) {         //??:30 --> ??:34
      SET_DISPLAY_WORD(W_HALF)
    }
    else if (inputMinute >= 15 && inputMinute <20 ) { //??:15 --> ??:19
      SET_DISPLAY_WORD(W_QUARTER)
    }
    else {
      if (inputMinute >= 20){       //??:20 --> ??:29
        SET_DISPLAY_WORD(W_TWENTY)
        if (inputMinute >= 25) {    //??:25 --> ??:29
          SET_DISPLAY_WORD(W_MFIVE)
        }
      }
      else if (inputMinute >= 10){  //??:10 --> ??:14
        SET_DISPLAY_WORD(W_MTEN)
      }
      else {                        //??:05 --> ??:09
        SET_DISPLAY_WORD(W_MFIVE)
      }
    }  
  }
  else {                            //??:35 --> ??:59
    inputHour++;
    SET_DISPLAY_WORD(W_TO)
      
    if (inputMinute >= 45 && inputMinute <50 ) { //??:45 --> ??:49
      SET_DISPLAY_WORD(W_QUARTER)
    }
    else {
      if (inputMinute < 45) {        //??:35 --> ??:44
        SET_DISPLAY_WORD(W_TWENTY)
          
        if (inputMinute < 40) {     //??:35 --> ??:39
          SET_DISPLAY_WORD(W_MFIVE)
        }
      }
      else if (inputMinute < 55) {   //??:50 --> ??:54
        SET_DISPLAY_WORD(W_MTEN)
      }
      else {                        //??:55 --> ??:59
        SET_DISPLAY_WORD(W_MFIVE)
      }
    }
  }
  
  switch(inputHour - (inputHour >= 12 ? 12 : 0)) {
    CASE_WORD(0,  W_TWELVE)
    CASE_WORD(1,  W_ONE)
    CASE_WORD(2,  W_TWO)
    CASE_WORD(3,  W_THREE)
    CASE_WORD(4,  W_FOUR)
    CASE_WORD(5,  W_FIVE)
    CASE_WORD(6,  W_SIX)
    CASE_WORD(7,  W_SEVEN)
    CASE_WORD(8,  W_EIGHT)
    CASE_WORD(9,  W_NINE)
    CASE_WORD(10, W_TEN)
    CASE_WORD(11, W_ELEVEN)
  }

  //set minute status
  for (int i=0; i < inputMinute % 5; ++i)
    pixels.setPixelColor(W_DOTS[i], setColour);
  
  pixels.show();
  delay(500);
}
